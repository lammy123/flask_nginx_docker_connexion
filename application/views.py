def post_greeting(name: str) -> str:
    return 'Hello {name}'.format(name=name)
